const express = require("express");
const mongodb = require("mongodb");

const router = express.Router();

// Get Posts
router.get("/", async (req, res) => {
  const posts = await loadUpdatedPostsCollection();
  res.send(await posts.toArray());
});

// Add Posts
router.post("/", async (req, res) => {
  const posts = await loadPostsCollection();
  const userData = {
    username: req.body.user.username
  };
  await posts.insertOne({
    content: req.body.formData,
    user: userData,
    createdAt: new Date()
  });
  res.status(201).send();
});

// Delete Posts
router.delete("/:id", async (req, res) => {
  const posts = await loadPostsCollection();
  await posts.deleteOne({ _id: new mongodb.ObjectID(req.params.id) });
  res.status(200).send();
});

// Like Post
router.post("/:id", async (req, res) => {
  const posts = await loadPostsCollection();
  const userId = req.body.userId;

  await posts
    .findOne({ _id: new mongodb.ObjectID(req.params.id) })
    .then(postItem => {
      if (
        postItem &&
        postItem.content.likes.find(item => item.id === userId.id)
      ) {
        posts.updateOne(
          { _id: new mongodb.ObjectID(req.params.id) },
          { $pull: { "content.likes": userId } }
        );
      } else {
        posts.updateOne(
          { _id: new mongodb.ObjectID(req.params.id) },
          { $push: { "content.likes": userId } }
        );
      }
    })
    .then(async () => {
      const updatedPost = await posts.findOne({
        _id: new mongodb.ObjectID(req.params.id)
      });
      res.status(200).send(updatedPost.content.likes);
    });
});

async function loadUpdatedPostsCollection() {
  const client = await mongodb.MongoClient.connect(
    "mongodb+srv://cloud_admin:t8JQ3P9PSbVEbhnD@cluster0-jvi6z.mongodb.net/test?retryWrites=true&w=majority",
    {
      // useNewUrlParser: true,
      useUnifiedTopology: true
    }
  );

  return client
    .db("test")
    .collection("posts")
    .aggregate([
      {
        $lookup: {
          from: "users",
          localField: "user.username",
          foreignField: "username",
          as: "userData"
        }
      },
      {
        $replaceWith: {
          $mergeObjects: [
            {
              _id: "$_id",
              content: "$content",
              user: {
                name: "$userData.name",
                avatar: "$userData.avatar",
                userId: "$userData._id"
              },
              createdAt: "$createdAt"
            }
          ]
        }
      },
      // take out user's data from an array in to object
      { $unwind: "$user" },
      { $unwind: "$user.name" },
      { $unwind: "$user.avatar" },
      { $unwind: "$user.userId" }
    ]);
}

async function loadPostsCollection() {
  const client = await mongodb.MongoClient.connect(
    "mongodb+srv://cloud_admin:t8JQ3P9PSbVEbhnD@cluster0-jvi6z.mongodb.net/test?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  );

  return client.db("test").collection("posts");
}

module.exports = router;
