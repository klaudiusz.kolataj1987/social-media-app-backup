const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");
const key = require("../../config/keys").secret;
const User = require("../../model/User");
const mongodb = require("mongodb");
const defaultAvatarImg = require("../../utils/default-avatar");

/**
 * @route POST api/users
 * @desc Get all users
 * @acces Public
 */

router.get("/", async (req, res) => {
  const allUsers = await loadUsersCollection();
  res.send(await allUsers.find({}).toArray());
});
/**
 * @route POST api/users/register
 * @desc Register the User
 * @acces Public
 */

router.post("/register", (req, res) => {
  let { name, username, email, password, confirm_password } = req.body;
  if (password !== confirm_password) {
    return res.status(400).json({
      msg: "Password do not match!"
    });
  }
  // Check for the uniqe Email
  User.findOne({ username: username }).then(user => {
    if (user) {
      return res.status(400).json({
        msg: "Username is already taken!"
      });
    }
  });

  // Check for the Uniqe Email
  User.findOne({ email: email }).then(user => {
    if (user) {
      return res.status(400).json({
        msg: "Email is already registered!. Did you forgot password?"
      });
    }
  });

  //The data is valid and now we can register the user
  let newUser = new User({
    name,
    username,
    password,
    email,
    avatar: defaultAvatarImg,
    userRole: "BASIC"
  });
  // Hash the password
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if (err) throw err;
      newUser.password = hash;
      newUser.save().then(user => {
        return res.status(201).json({
          success: true,
          msg: "User is now registered!"
        });
      });
    });
  });
});

/**
 * @route POST api/users/login
 * @desc Login the User
 * @acces Public
 */
router.post("/login", (req, res) => {
  User.findOne({ username: req.body.username }).then(user => {
    if (!user) {
      return res.status(404).json({
        msg: "Username is not found.",
        succes: false
      });
    }

    // if there is user we are now going to compare password
    bcrypt.compare(req.body.password, user.password).then(isMatch => {
      if (isMatch) {
        // User's password is correct and we need to send the JSON Token for that user
        const payload = {
          _id: user._id,
          username: user.username,
          name: user.name,
          email: user.email,
          userRole: user.userRole
        };
        jwt.sign(payload, key, { expiresIn: 604800 }, (err, token) => {
          res.status(200).json({
            success: true,
            user: user,
            token: `Bearer ${token}`,
            msg: "You are now logged!"
          });
        });
      } else {
        return res.status(404).json({
          msg: "Incorrect password!.",
          succes: false
        });
      }
    });
  });
});

/**
 * @route GET api/users/profile
 * @desc Return the User's data
 * @acces Private
 */

router.get(
  "/profile",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let userData = res.json({
      user: {
        avatar: req.user.avatar,
        userId: req.user._id,
        username: req.user.username,
        name: req.user.name,
        email: req.user.email,
        userRole: req.user.userRole
      }
    });
    return userData.json();
  }
);

/**
 * @route POST api/users/profile/update
 * @desc Update the User's data
 * @acces Private
 */

router.post("/profile/update", async (req, res) => {
  const users = await loadUsersCollection();
  const userId = req.body.id;
  const dataAvatar = req.body.avatar;
  await users.findOne({ _id: new mongodb.ObjectID(userId) }).then(() => {
    users.updateOne(
      { _id: new mongodb.ObjectID(userId) },
      {
        $set: { avatar: dataAvatar }
      },
      { upsert: true }
    );
  });

  res.status(200).send();
});

async function loadUsersCollection() {
  const client = await mongodb.MongoClient.connect(
    "mongodb+srv://cloud_admin:t8JQ3P9PSbVEbhnD@cluster0-jvi6z.mongodb.net/test?retryWrites=true&w=majority",
    {
      useNewUrlParser: false,
      useUnifiedTopology: true
    }
  );
  return client.db("test").collection("users");
}

module.exports = router;
