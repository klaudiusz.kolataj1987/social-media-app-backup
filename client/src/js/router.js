import Vue from "vue";
import Router from "vue-router";

// import Login from "../views/Login.vue";
import Home from "../views/Home.vue";
import store from "../store";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../views/Login.vue"),
      meta: {
        requiresGuest: true
      }
    },
    {
      path: "/register",
      name: "register",
      component: () => import("../views/Register.vue"),
      meta: {
        requiresGuest: true
      }
    },
    {
      path: "/profile",
      name: "profile",
      component: () => import("../views/Profile.vue"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/admin-panel",
      name: "admin-panel",
      component: () => import("../views/UserPanel.vue"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/logout",
      name: "logout",
      component: () => import("../views/Logout.vue")
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.isLoggedIn) {
      // redirect to login page
      next("/login");
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.requiresGuest)) {
    if (store.getters.isLoggedIn) {
      // redirect to profile page
      next("/profile");
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
