import Vue from "vue";
import Vuex from "vuex";
import PostService from "@/PostService";
// import ProfileService from "@/ProfileService";

import Auth from "@/Warehouse/Auth";

Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    Auth
  },
  state: {
    count: 0,
    showDialog: false,
    redirectLoginPage: false,
    posts: [],
    users: [],
    postIndex: -1,
    choosedPosts: [],
    images: [],
    userData: {},
    progressBar: 0
  },
  mutations: {
    progressPostImage(state, payload) {
      state.progressBar = payload;
    },
    clearProgressBar(state) {
      state.progressBar = 0;
    },
    toggleShowModal(state, payload) {
      const modalName = payload;
      switch (modalName) {
        case "addPost":
          state.showDialog = !state.showDialog;
          break;
        case "redirectLogin":
          state.redirectLoginPage = !state.redirectLoginPage;
          break;
      }
    },

    changeSpinnerState(state, payload) {
      state.postIndex = payload ? payload.index : -1;

      payload
        ? state.choosedPosts.push(payload.likeItem || payload.deleteItem)
        : console.log("empty");
    },

    async updatePosts(state) {
      state.posts = await PostService.getPosts().then(data => {
        state.postIndex = -1;
        state.choosedPosts.splice(0);
        return data.sort(function(a, b) {
          a = new Date(a.createdAt);
          b = new Date(b.createdAt);
          return a > b ? -1 : a < b ? 1 : 0;
        });
      });
    },

    async getUsers(state) {
      try {
        state.users = await PostService.getUsers();
      } catch (err) {
        this.error = err.message;
      }
    },

    updateLikedOnlyPosts(state, payload) {
      const postId = payload.postId;
      const updatedLikes = payload.updatedLikes;

      // update like's state of each posts
      for (let i = 0; i < state.posts.length; i++) {
        state.posts[i]._id === postId
          ? (state.posts[i].content.likes = updatedLikes)
          : state.posts[i].content.likes;
      }
      state.postIndex = -1;
      state.choosedPosts.splice(0);
    }
  },
  actions: {
    async getPosts(state) {
      try {
        state.posts = await PostService.getPosts();
      } catch (err) {
        this.error = err.message;
      }
    },
    async createPost(context, payload) {
      await PostService.insertPost(payload.formData, payload.user);
      this.posts = await PostService.getPosts();
      context.commit("updatePosts");
      context.commit("toggleShowModal", "addPost");
    },

    async deletePost(context, payload) {
      context.commit("changeSpinnerState", payload);
      await PostService.deletePost(payload.id).then(() =>
        context.commit("updatePosts", {
          index: payload.index,
          deleteItem: payload.deleteItem
        })
      );
    },

    likePost(context, payload) {
      context.commit("changeSpinnerState", payload);
      return PostService.likePost(payload.id, payload.userData);
    },

    likePostAndFetchData(context, payload) {
      return context.dispatch("likePost", {
        id: payload.id,
        userData: payload.userData,
        index: payload.index,
        likeItem: { likeItemId: payload.id }
      });
    },

    async deletePostAndFetchData(context, payload) {
      await context.dispatch("deletePost", {
        id: payload.id,
        index: payload.index,
        deleteItem: { deleteItemId: payload.id }
      });
    }
  }
});
