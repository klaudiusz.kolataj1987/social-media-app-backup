import axios from "axios";
import store from "./store";
const url = "api/posts";
const usersUrl = "api/users";

class PostService {
  // Get Posts
  static getPosts() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(url);
        const data = res.data;
        resolve(
          data.map(post => ({
            ...post,
            createdAt: new Date(post.createdAt)
          }))
        );
      } catch (err) {
        reject(err);
      }
    });
  }
  // users to refactor
  static getUsers() {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await axios.get(usersUrl);
        const data = res.data;
        resolve(
          data.map(user => ({
            ...user,
            createdAt: new Date(user.createdAt)
          }))
        );
      } catch (err) {
        reject(err);
      }
    });
  }

  // Like Post
  static likePost(id, userId) {
    return axios.post(`${url}/${id}`, { userId });
  }

  // Create Posts
  static insertPost(formData, user) {
    return axios.post(
      url,
      { formData, user },
      {
        onUploadProgress: uploadEvent => {
          let counter = ~~((uploadEvent.loaded / uploadEvent.total) * 100);
          store.commit("progressPostImage", counter);
        }
      }
    );
  }

  // Delete Posts
  static deletePost(id) {
    return axios.delete(`${url}/${id}`);
  }
}

export default PostService;
