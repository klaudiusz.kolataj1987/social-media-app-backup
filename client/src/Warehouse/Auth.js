import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import router from "../js/router";

Vue.use(VueAxios, axios);

const state = {
  token: localStorage.getItem("token") || "",
  user: {},
  status: "",
  error: null
};

const getters = {
  isLoggedIn: state => !!state.token,
  authState: state => state.status,
  user: state => state.user,
  error: state => state.error
};

const actions = {
  // Login action
  async login({ commit }, user) {
    commit("auth_request");
    try {
      let res = await axios.post("/api/users/login", user);
      if (res.data.success) {
        const { name, username, _id, email, date, userRole } = res.data.user;
        const userData = {
          name,
          username,
          _id,
          email,
          date,
          userRole
        };
        const token = res.data.token;
        const user = userData;
        // Store the token into the localStorage
        localStorage.setItem("token", token);
        // Set axios defaults
        axios.defaults.headers.common["Authorization"] = token;
        commit("auth_success", { token, user });
      }
      return res;
    } catch (err) {
      commit("auth_error", err);
    }
  },

  // Register action
  async register({ commit }, userData) {
    try {
      commit("register_request");
      let res = await axios.post("/api/users/register", userData);
      if (res.data.success !== undefined) {
        commit("register_succes");
      }
      return res;
    } catch (err) {
      commit("register_error", err);
    }
  },

  // Get user profile
  async getProfile({ commit }) {
    commit("profile_request");
    let res = await axios.get("/api/users/profile");
    commit("user_profile", res.data.user);
    return res;
  },

  // Update user profile
  updateProfile(commit, payload) {
    return axios.post("/api/users/profile/update", {
      avatar: payload.avatar,
      id: payload.id
    });
  },

  //logout the user
  async logout({ commit }) {
    await localStorage.removeItem("token");
    commit("logout");
    delete axios.defaults.headers.common["Authorization"];
    router.push("/login");
    return;
  }
};

const mutations = {
  auth_request(state) {
    state.status = "loading";
  },

  auth_success(state, payload) {
    state.token = payload.token;
    state.user = payload.user;
    state.status = "success";
  },
  auth_error(state, err) {
    state.error = err.response.data.msg;
  },
  register_request(state) {
    state.error = null;
    state.status = "loading";
  },
  register_succes(state) {
    state.error = null;
    state.status = "success";
  },
  register_error(state, err) {
    state.error = err.response.data.msg;
  },
  logout(state) {
    state.error = null;
    state.status = "";
    state.token = "";
    state.user = "";
    state.userRole = "";
  },
  profile_request(state) {
    state.status = "loading";
  },

  user_profile(state, user) {
    state.user = user;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
