import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import store from "./store";
import router from "./js/router";
import App from "./App.vue";
import Vuelidate from "vuelidate";
Vue.use(Vuelidate);
import axios from "axios";

import VueMaterial from "vue-material";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css";

// Setting up default vue's http modules for api calls
Vue.prototype.$https = axios;
// load the token from localStorage
const token = localStorage.getItem("token");
// if there is any token then we will simply append default axios authorization headers
if (token) Vue.prototype.$http.defaults.headers.common["Authorization"] = token;

Vue.use(VueMaterial);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
