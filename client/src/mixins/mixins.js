export const convertToBase64 = {
  methods: {
    convertToBase64(event) {
      return new Promise((resolve, reject) => {
        var fr = new FileReader();
        fr.onerror = reject;
        fr.onload = () => {
          resolve(fr.result);
        };
        fr.readAsDataURL(event.target.files[0]);
      });
    }
  }
};
